<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('FS_METHOD', 'direct');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test_wordpress');

/** MySQL database username */
define('DB_USER', 'anusharan');

/** MySQL database password */
define('DB_PASSWORD', 'Anusharan@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'B,PVx47=&>s 0~R`@b%F_8l)t%xh4Hpx|q8((a!+RVrpiV 2:p~_[DuVrcN6. wt');
define('SECURE_AUTH_KEY', 'u`T=H!Pp~:{%t5CCf(vuFtC>L2>m%f5u]#]g&A_:2 H&qrstYWqdSyP4WpW<HNS8');
define('LOGGED_IN_KEY', 'm}KGUgXJxNt4{n{sYC67az&TrIHd:l.TF:xh2wHx3;2F_=li5pwJ%RlM/q %.iyE');
define('NONCE_KEY', 'vkI7:ytU.&(,$IcoZ^Ky{Tc9R-/&<cbl)C2QdHI*UV6Ym*&j2$YyKpq3z!wFR[B*');
define('AUTH_SALT', 'mZ(@M$Jx@m&1-2wAEQLay?K%GHzO[Rw2+EHf@c-pAP5IhVUIosHNFrj/Wy8WpCD&');
define('SECURE_AUTH_SALT', '8*E@Bgl7FtR7IJCQll81e9tRjdj1gpuw;TZ3)1I]#fBCC`P[AE=AJ!ac=?hB4N<[');
define('LOGGED_IN_SALT', 'ZBD==G`MP2Z}4*oN$iG+S@9-bndIu^OOL:!Q)9IAxbG,S;a}/aQi4@%f_LI)yp9X');
define('NONCE_SALT', 'J{UJ.q{zlK]_voHC{6oxqeORKVJ;LQo1sc !i ,~r3D#:|ab3+c48jr8L_?|PH?}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';